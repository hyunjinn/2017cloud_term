from operator import itemgetter
import networkx as nx

import matplotlib
matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt

node_sizes = []
labels = {}
max_size = range(102)

g = nx.Graph()


for key in max_size:
	node_sizes.append(max_size)
	g.add_node(str(key))
	g.add_edge(0,str(key),weight=100/(key+1))
	

print nx.info(g)
nx.draw_networkx(g,with_labes = True, node_size = node_sizes)



plt.show()