import numpy as np
import time

from operator import itemgetter
import networkx as nx
import matplotlib.pyplot as plt

f = open('author.txt', 'r')
lines = f.read().splitlines()
tmp = []
elements = []
authors = []
year = 0
citation = []
node_size = []
citations = []
node_sizes = []
idxs = []


#read reference text file
for line in lines:
	# line for indexing (numbering)
	if line.startswith("NO  - "):
		idx = int(line.strip("NO  - "))
		idxs.append(idx)


	# line for author(s)
	if line.startswith("AU  - "):
		authors.append(line.strip("AU  - "))

	if line.startswith("CT  - "):
		citation = int(line.strip("CT  - "))
		citations.append(citation)	

	# line for publication year
	if line.startswith("PY  - "):
		year = int(line.strip("PY  - ").strip("///"))
		for author in authors:
			tmp.append(idx)
			tmp.append(author)
			tmp.append(citation)
			tmp.append(year)
			elements.append(tmp)
			tmp = []
		authors = []

g = nx.Graph()

colormap = []
for key in idxs:
	if key==0:
		continue
	g.add_edge(0,key)

for cit in citations:
	size = cit/10
	node_sizes.append(size)
	
	if size > 500 and size <1000:
		colormap.append("yellow")
	elif size > 1000:
		colormap.append("red")
	else:
		colormap.append("skyblue")

pos=nx.spring_layout(g) # positions for all nodes
nx.draw_networkx(g, pos = pos, node_size = node_sizes, node_color = colormap, with_labels=True)


plt.show()









